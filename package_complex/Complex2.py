
class Complex:
    def __init__(self, a, b):
        self.a = float(a)
        self.b = float(b)
    
    def __abs__(self):
        return float((self.a*self.a + self.b*self.b)**(1/2))
    
    def __add__(self, other):
        self.a = self.a + other.a
        self.b = self.b + other.b
        return Complex(self.a, self.b)

    def __sub__(self, other):
        self.a = self.a - other.a
        self.b = self.b - other.b
        return Complex(self.a, self.b)
    
    def __str__(self):
        return "%r + %ri" % (self.a, self.b)

    def __repr__(self):
        return "%r + %ri" % (self.a, self.b)
    
    def __mul__(self, other):
        if type(other) == Complex:
            return Complex(self.a*other.a - self.b*other.b, self.a*other.b + other.a*self.b)

        elif type(other) == float or type(other) == int:
            return Complex(self.a * other, self.b * other)

        else:
            TypeError('Nasobit musis cislem, trubko!')

    def __bool__(self):
        return bool(self.b)



